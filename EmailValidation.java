
import java.util.Scanner;
/**
 * 
 * @author Mohamed Mahmoud Mohamed Vall
 */
public class EmailValidation {
            public static void main (String[] args){
                
                System.out.println("Please insert your email below"); 
                
                Scanner sc = new Scanner(System.in);
                String useremail = sc.next(); 
                 
                int valid=0;
                /*Think of "valid" as a condition meter. Everytime the inserted 
                    email meets one of the condition, its validness increases
                */
                for (int i=0; i<=useremail.length()-1; i++){
                    if ((useremail.charAt(i)=='@'))
                    valid++;
                    if ((useremail.charAt(i) == '.' ))
                            valid++;
                    
                }
                /*This loop scans every character of the email and sees if 
                    they're an "@". If so, the loop continues by looking for
                    a dot.
                */
                if(valid==0){
                    System.out.println("Sorry, but "+useremail+" isn't a valid"
                            + "email. Try again next time.");
                }else if(valid==1){
                    System.out.println(useremail+ " is not a valid email."
                            + "Did you forget to write the domain?");
                }else{
                    System.out.println(useremail+" is a valid email.");
                }
                /*I thought it would be fun to write a different text if the
                    conditions are only halfway met, that way whoever
                    uses this program will realize their mistake
                */
            }
}
}