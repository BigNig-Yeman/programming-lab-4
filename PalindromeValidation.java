
import java.util.Scanner;

public class PalindromeValidation {
        public static void main (String[] args){
        
            Scanner sc = new Scanner(System.in);
            System.out.println("Insert your sentence please.");
            String specialword = sc.nextLine();
            
            String spacelessword = specialword.replaceAll("\\s","");
          /* <<.replaceAll("\\s","")>> is a very handy dandy function
            that essentially removes all spaces within a string. To use it,
            write the string object +.replaceAll("\\s","") and assign this
            modified string to a different class.
           Make sure the "s" is lowercase, otherwise the code will remove
            all non-space characters instead*/
          
          String charactersonly = spacelessword.replaceAll("\\W","");
          /*Another neat function, replaceAll("\\W","") removes every
          character that isn't a word character (such as punctuations).
          For some reason, it does not remove numbers included in the string.
          Once again, this code is case sensitive, so make sure you don't type
          lowercase w, otherwise every word character will be removed
          */
          
          /*
          You can convert the lenght of a string into an int with
          the ".length" function
          */
          
          String reversed = "";
           for(int length =  charactersonly.length() - 1; length >= 0; length--)
        {
            reversed = reversed + charactersonly.charAt(length);
        }
           /*In brief, this loop essentially scans the modified inserted
          sentence and adds one character to our new word every 
           cycle, starting from the last one.
           */
            if(specialword.equalsIgnoreCase(reversed))
            
          /*The ".equalsIgnoreCase() function allows us to compare
          two different strings without taking into consideration 
          the difference in capitulation
          */
            {
                System.out.println(specialword+" is a palindrome, meaning"
                        + "that it's spelled the same backwords.");
            }else{
                System.out.println("Hmm, I don't think " +specialword+
                        " is a palindrome. I think it's just an ordinary word...");
            }
        }
}
       
