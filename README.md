# Programming Lab 4

4th Lab of this Programming class.
EmailValidation is a program that analyzes a given string and sees if said string includes 
the prerequisites of an email (an @ and a .). An issue with this program is that it won't 
check for characters other than the prerequisites one, so it'll accept not only emails that 
don't have two words after the @, but also mails that don't have any word at all.

PalindromeValidarion uses loops to recreate an inserted string in a reversed, spaceless
version with only letters and number in order to find out wether or not it truly is a
palindrome. As far as I could tell, this program doesn't have any issues

RandomNumberGuessing generates a random number and tasks the user with guessing what said
number is. Although the program manages to react accordingly if the user guesses right or
wrong and if the limit of trials has been exceeded and even asks the user if they want to
retry or not, it fails to actually wait for the user's answer, as it just skips past that 
part and immedietally asks them to try again.