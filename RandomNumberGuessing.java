
import java.util.Random;
import java.util.Scanner;

public class RandomNumberGuessing {
            public static void main (String [] args) {
                    
                    Scanner sc = new Scanner(System.in);
                    
                    int randomnumber =( new Random()).nextInt(101);
                  
                    /*I assigned 101 instead of 100 as the higher limit, 
                    as otherwise, Java would get a random number 
                    between 0 and 99*/
                    
                    System.out.println("Can you guess what my random number is?"
                            + "Hint: It's an interger between 0 and 100.");
                    
                    int numberoftries = 0;
                    int i=0;
                    
                    while (numberoftries != 10 && i==0){
                              System.out.println("Insert your guess below.");
                              int guessednumber = sc.nextInt();
                              numberoftries++;
                        if(guessednumber == randomnumber){
                              System.out.println("Congratulations, you guessed right!"
                                + "And it only took you " +numberoftries+ " tries");
                              i++;
                        }
                        /*If the user guesses right, the variable i will be 
                           increased by one, breaking one of the conditions
                           and freeing the user from the loop
                        */
                        else if (guessednumber != randomnumber){ 
                              System.out.println("Bzzt! Wrong answer!"
                                    + "Would you like to try again? Yes or No?");
                             String retryornot = sc.nextLine();
                            /*If the user fails to guess however, he will be
                               asked wether or not they want to retry or not
                             */
                        if("Yes".equalsIgnoreCase(retryornot))
                                System.out.println("Give it your best guess");
                             
                        else if("No".equalsIgnoreCase(retryornot)){
                              System.out.println("Ah, that's a shame. "
                                        + "Better luck next time!");
                              i++;

                              /*Thanks to the "equalsIgnoreCase", the program 
                                  will (or at least) should take "yes" and "no" as
                                  valides answers
                              */
                        }
                    }
            }
                System.out.println("Thank you for your time!");
}
}
